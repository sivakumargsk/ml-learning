import math as math


def manhattan_distance(point1, point2):
    return abs(point1[0] - point2[0]) + abs(point1[1] - point2[1])


def euclidean_distance(point1, point2):
    side_a = (point1[0] - point2[0]) ** 2
    side_b = (point1[1] - point2[1]) ** 2
    return math.sqrt(side_a + side_b)


def chebyshev_distance(point1, point2):
    return max(abs(point1[0] - point2[0]), abs(point1[1] - point2[1]))


def correlation_coefficient(data_x, data_y):
    n = len(data_x)
    sigma_x = sum(data_x)
    sigma_y = sum(data_y)
    sigma_xy = sum(map(lambda i, j: i*j, data_x, data_y))
    sigma_xx = sum(map(lambda i: i*i, data_x))
    sigma_yy = sum(map(lambda i: i*i, data_y))
    numerator = (n * sigma_xy) - (sigma_x * sigma_y)
    denominator = math.sqrt(((n * sigma_xx) - (sigma_x ** 2))
                            * ((n * sigma_yy) - (sigma_y ** 2)))
    return numerator / denominator
