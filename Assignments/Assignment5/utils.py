import math as math


def pearson_r(data_x, data_y):
    n = len(data_x)
    sigma_x = sum(data_x)
    sigma_y = sum(data_y)
    sigma_xy = sum(data_x * data_y)
    sigma_xx = sum(data_x * data_x)
    sigma_yy = sum(data_y * data_y)
    numerator = (n * sigma_xy) - (sigma_x * sigma_y)
    denominator = math.sqrt(((n * sigma_xx) - (sigma_x ** 2))
                            * ((n * sigma_yy) - (sigma_y ** 2)))
    return numerator / denominator
