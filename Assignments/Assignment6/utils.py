def normalise(min_r, max_r, coll):
    range_r = max_r - min_r
    return (coll - min_r)/range_r


def normalise_ratings(ratings):
    result = ratings.copy()
    result['IMDB'] = normalise(0, 10, result['IMDB'])
    result['RT'] = normalise(0, 10, result['RT'])
    result['RV'] = normalise(0, 4, result['RV'])
    result['RE'] = normalise(0, 4, result['RE'])
    result['MC'] = normalise(0, 10, result['MC'])
    result['TG'] = normalise(0, 5, result['TG'])
    result['AC'] = normalise(0, 4, result['AC'])
    result['T'] = normalise(0, 1, result['T'])
    return result
